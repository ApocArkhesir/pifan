This repository contains files needed to regulate Raspberry Pi 3B+ HAT fan. 
Without this configuration the fan is going to spin loudly at full speed every now and then.

# Usage
1. `sudo make install`
2. Start and enable `pifan.service`.

