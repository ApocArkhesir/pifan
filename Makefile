# Default target
.DEFAULT_GOAL := install

_check_if_installable:
	[ -d /usr/local/bin ]

# MAIN TARGETS
install: _check_if_installable
	cp ./pifan.sh /usr/local/bin
	cp ./pifan.service /etc/systemd/system/
	cat ./boot/config.txt >> /boot/config.txt
	chmod 755 /usr/local/bin/pifan.sh
	systemctl daemon-reload

